const myform = document.getElementById('myform')
const title = document.getElementById('title')
const body = document.getElementById('body')
const color = document.getElementById('color')

const searchForm = document.getElementById('search_form')
const search = document.getElementById('search_query')
const btnSearch = document.getElementById('btn-search')

// if(search.value.length === 0){
//   btnSearch.disabled = 'disabled'
// }


const articles = document.getElementById('articles')
const searched = document.getElementById('searched')

function addRecord(newData) {
  fetch('http://127.0.0.1:5000/add', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(newData)
  })
    .then((res) => res.json())
    .then((data) => {
      // console.log(data)
      getAllData()
    })
    .catch((err) => console.log(`Error in fetch post-add: ${err}`))
}

const getAllData = () => {
  fetch('http://127.0.0.1:5000/get', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(res => res.json())
    .then((data) => {
      console.log(data)
      renderArticles(data)
    })
    .catch((err) => console.log(`Error in fetch get all: ${err}`))
}

getAllData() // render all records

function deleteRecord (id) {
    fetch(`http://127.0.0.1:5000/delete/${id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .then((data) => getAllData())
}

function getDataById(id) {
  fetch(`http://127.0.0.1:5000/get/${id}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(res => res.json())
  .then((data) => renderOneItem(data))
}

function searchText(text) {
  fetch(`http://127.0.0.1:5000/search/${text}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
    // body: JSON.stringify(data)
  })
    .then((res) => res.json())
    .then((data) => {
      // console.log(data)
      renderSearched(data)
    })
    .catch((err) => console.log(`Error in fetch post-add: ${err}`))
}

function renderOneItem(item) {
  // console.log(item)
  title.value = item.title
  body.value = item.body
  color.value = item.color
}

function renderArticles (myData) {
  articles.innerHTML = ''
  myData.forEach((item) => {
    articles.innerHTML +=
      `
        <div class="card card-body my-2">
          <h2 class="card-title">${item.title}</h2>
          <p>${item.body}</p>
          <h6>${String(item.date).replace('T', " - ")}</h6>
          <p>
            <button class="btn btn-danger" onclick="deleteRecord(${item.id})">Delete</button>
            <button class="btn btn-success" onclick="getDataById(${item.id})">Update</button>
          </p>
        </div>
      `
  })
}

function renderSearched (myData) {
  searched.innerHTML = ''
  myData.forEach((item) => {
    searched.innerHTML +=
      `
        <div class="card card-body my-2">
          <h2 class="card-title">${item.title}</h2>
          <p>${item.body}</p>
          <h6>${String(item.date).replace('T', " - ")}</h6>
        </div>
      `
  })
}

function handleSubmit(e) {
  e.preventDefault()
  // console.log('Hello World')
  const newData = {
    title: title.value,
    body: body.value,
    colors: color.value
  }

  addRecord(newData)
  myform.reset()
}

function handleSubmitSearch(e) {
  e.preventDefault()

  // console.log(search.value)
  if(search.value.length !== 0) {
    searchText(search.value)
    searchForm.reset()
  }
}

myform.addEventListener('submit', (e) => handleSubmit(e))
searchForm.addEventListener('submit', (e) => handleSubmitSearch(e))