## Python-Electron-Flask

https://youtu.be/povYTkpJiRA

### Cоздать и активировать виртуальное окружение:
```python
python3 -m venv venv

source venv/bin/activate

python3 -m pip install --upgrade pip
```

git reset


### Установить зависимости из файла requirements.txt:
```python
pip3 install -r requirements.txt

pip install Flask-SQLAlchemy
pip install mysqlclient
pip install flask-marshmallow
pip install marshmallow-sqlalchemy
# pip3 install python-dotenv
```
### If port is busy
```shell
lsof -P -i :5000

```
### Record library tu requirements.txt
```python
pip3 freeze > requirements.txt
```
### For check pip
```shell
pip list
```

### start project
```python
python3 main.py 
```
### Let's install ElectronJS
```shell
npm install --save-dev electron
npm install --save-dev electronmon
npm install --save-dev electron-packager
npm install python-shell   - <IMPORTANT NOT --save-dev>
```
### For checking npm
```shell
npm list
```
### for start App in devMode
```
npx electron-packager .
```

### for packaging App in usebel Op-System
```
npx electron-packager .
```