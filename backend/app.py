from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
# from sqlalchemy import text
from flask_marshmallow import Marshmallow
import datetime


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:admin@localhost/flask_test'
#  'mysql://root:admin@localhost/flask_test' - root is username and flask_test is database-name
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy()
db.init_app(app)
# OR db = SQLAlchemy(app)
ma = Marshmallow(app)


# class Colors(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     colors = db.Column(db.String(20), nullable=False)
#     goods_id = db.Column(db.Integer, db.ForeignKey('goods.id'), nullable=False)


class Goods(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    body = db.Column(db.String(100))
    # colors = db.relationship('Colors', backref='goods', lazy=True)
    colors = db.Column(db.String(100))
    date = db.Column(db.DateTime, default=datetime.datetime.now)

    def __init__(self, title, body, colors):
        self.title = title
        self.body = body
        self.colors = colors


class GoodsSchema(ma.Schema):
    class Meta:
        fields = ('id', 'title', 'body', 'colors', 'date')


good_schema = GoodsSchema()
goods_schema = GoodsSchema(many=True)


# first creating table in db ( need to install <brew install mysql>) after in terminal -> Python3 -> from app import db
# https://flask-sqlalchemy.palletsprojects.com/en/3.0.x/quickstart/
with app.app_context():
    db.create_all()


@app.route('/get', methods=['GET'])
def get_goods():
    all_goods = Goods.query.all()
    # all_articles = Articles.query(filter(id == 29))
    results = goods_schema.dump(all_goods)
    return goods_schema.jsonify(results)
    # return jsonify({'Hello': 'World'}) # test record


@app.route('/get/<id>', methods=['GET'])
def post_details(id):
    # article = Articles.query.get(id)
    good = db.get_or_404(Goods, id)
    return good_schema.jsonify(good)


@app.route('/add', methods=['POST'])
def add_article():
    title = request.json['title']
    body = request.json['body']
    colors = request.json['colors']

    goods = Goods(title, body, colors)
    db.session.add(goods)
    db.session.commit()
    return good_schema.jsonify(goods)


@app.route('/update/<id>', methods=['PUT'])
def update_article(id):
    good = db.get_or_404(Goods, id)

    title = request.json['title']
    body = request.json['body']
    colors = request.json['colors']

    good.title = title
    good.body = body
    good.colors = colors
    db.session.commit()
    return good_schema.jsonify(good)


@app.route('/delete/<id>', methods=['DELETE'])
def article_delete(id):
    good = db.get_or_404(Goods, id)
    db.session.delete(good)
    db.session.commit()
    # return ({'result of delete': 'success'})
    return good_schema.jsonify(good)  # return deleted object


@app.route('/search/<text>', methods=['GET'])
def search_query(text):
    # print("receiving request with text: text")
    # query = text('SELECT * FROM flask_test.goods')
    searching = f'%{text}%'
    # goods = db.session.execute(db.select(Goods).filter(title="Title 2").order_by(Goods.date)).scalars()
    # https://www.programmersought.com/article/46979687035/
    goods = db.session.execute(db.select(Goods).filter(db.or_(Goods.body.like(searching), Goods.body.like(searching))).limit(3)
                               .order_by(Goods.date.asc())).scalars().all()
    # goods = Goods.query.filter(db.or_(Goods.body.like(searching), Goods.body.like(searching))).all()
    return goods_schema.jsonify(goods)


if __name__ == '__main__':
    # app.run(debug=False)
    app.run(debug=True)
