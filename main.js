const { app, BrowserWindow, ipcMain, Menu} = require('electron');
const path = require('path');
// const os = require("os");
let { PythonShell } = require("python-shell");
// import {PythonShell} from 'python-shell';

process.env.NODE_ENV = 'production1'
// process.env.PATH = path.join(__dirname, '/python')
console.log(process.env.PATH)

const isDev = process.env.NODE_ENV !== 'production';
const isMac = process.platform === 'darwin';

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
// if (require('electron-squirrel-startup')) {
//   app.quit();
// }

let mainWindow
let streamWindow

const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: isDev ? 1200 : 1600,
    // width: 600,
    height: 600,
    title: "CALCULATOR",
    // icon: `${__dirname}/icons/mac/icon.icns`,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: true,
      // preload: path.join(__dirname, '/preload.js'),
    },
  });

  // and load the index.html of the app.
  mainWindow.loadFile('src/ui/index.html');

  // Open the DevTools.
  if(isDev) {
    mainWindow.webContents.openDevTools();
  }
  const mainMenu = Menu.buildFromTemplate(menu);
  Menu.setApplicationMenu(mainMenu);
};

const menu = [
  ...(isMac
    ? [
        {
          label: 'File',
          submenu: [
            {
              label: 'Get Article',
              // click: createAboutWindow,
            },
            {
              label: 'Exit',
              click: () => app.quit(),
            }
          ],
        },
        {label: "About"}
      ]
    : []),
  // {
  //   role: 'fileMenu',
  // },
  ...(!isMac
    ? [
        {
          label: 'File',
          submenu: [
            {
              label: 'Get Article',
              // click: createAboutWindow,
            },
            {
              label: 'Exit',
              click: () => app.quit(),
            }
          ],
        },
        {label: "About"}
      ]
    : []),
  {
    label: 'About',
    submenu: [
      {
        label: 'About app',
        // click: () => app.quit(),
        // accelerator: 'CmdOrCtrl+W',
      },
    ],
  },
  ...(isDev
    ? [
        {
          label: 'Developer',
          submenu: [
            { role: 'reload' },
            { role: 'forcereload' },
            { type: 'separator' },
            { role: 'toggledevtools' },
          ],
        },
      ]
    : []),
];

let options = {
  mode: 'text',
  pythonPath: '/Users/olegvpc/Web/FW-Electron/python-electron-mysql/venv/bin/python3',
  pythonOptions: ['-u'], // get print results in real-time
  scriptPath: '/Users/olegvpc/Web/FW-Electron/python-electron-mysql/backend/',
  args: null
};
PythonShell.run('app.py', options)
  .then((messages)=> {
    console.log(messages)
    // mainWindow.webContents.send("python:result", messages)
  })
  .catch((err) => console.log(err))

app.whenReady().then(() => {
  createWindow()
})