## Python-Electron-Flask

![ScreenShot App](https://ic.wampi.ru/2023/03/02/App-Python-Flasc.png)


![ScreenShot BD MySQL](https://im.wampi.ru/2023/03/02/SNIMOK-EKRANA-2023-03-02-V-18.25.09.png)


### Cоздать и активировать виртуальное окружение:
```python
python3 -m venv venv

source venv/bin/activate

python3 -m pip install --upgrade pip
```
### Установить зависимости из файла requirements.txt:
```python
pip3 install -r requirements.txt
```


### If port is busy
```shell
lsof -P -i :5000
sudo kill <PID>
```
### Record library tu requirements.txt
```python
pip3 freeze > requirements.txt
```
### For check pip
```shell
pip list
mast be next:
pip install Flask-SQLAlchemy
pip install mysqlclient
pip install flask-marshmallow
pip install marshmallow-sqlalchemy
\
```

### Let's install ElectronJS
```shell
npm install
```

### For checking npm
```shell
npm list

npm install --save-dev electron
npm install --save-dev electronmon
npm install --save-dev electron-packager
npm install python-shell   - <IMPORTANT NOT --save-dev>
```
### for start App in devMode
```
npm run dev
```

### for packaging App in usable Operation-System
```
npm run make
```


